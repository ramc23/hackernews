
#Hacker News



## Tech Stack

**Server:** Node, Express,Sequelize,Postgres,Docker


## Deployment
Before when you clone the repository, you should do.
```bash
  npm install 
```
 To run our node service, we can type in root directory the application will run in the port 8080 and internal 80:
```bash
 docker-compose up 
```
To remove the container 
```bash
 docker-compose down   
```


## Check the Documentation 
 For to see the endpoints click here 


[Documentation](https://documenter.postman.com/preview/6594963-5290a989-f186-4257-aa9d-cf98834601e1?environment=&versionTag=latest&apiName=CURRENT&version=latest&documentationLayout=classic-double-column&right-sidebar=303030&top-bar=FFFFFF&highlight=EF5B25)

