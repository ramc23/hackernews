const express =require('express');
require('dotenv').config();
const cors = require('cors')
const bodyParser = require('body-parser')
const db =require('./db/connection');
  const {checkDb}= require('./db/seedDb'); 
  
const app=express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

   checkDb(); 

//Rutas 
app.get('/', (req, res) => {
    res.json({
        ok: true,
        msg: 'Hello World',
    })
})


app.use('/api/news',require('./routes/hits'));
//app.use('/api/user',require('./routes/user'));


app.listen(process.env.PORT, () => {
    console.log('Listening in port http://localhost:' + process.env.PORT);
});
