const { request, response } = require("express");
const { db } = require("../db/connection");
const hits = require("../models/hits");
const { QueryTypes } = require('@sequelize/core');
const { queryHits } = require('../db/query')
const{paginate} = require('../helpers/helpers')
  
const getHits = async (req, res) => {
  // Get all hits from the database
  try {

    // pagination 
    const { page, } = req.query
    const news = await db.query(
     ` ${queryHits} ${paginate(page)} `
      , {
        type: QueryTypes.SELECT,
        plain: false,
      });

    res.json(news);
  } catch (error) {
    res.status(404).json({
      error: "Not found"

    });
  }


}

const getHitsByAuthor = async (req, res) => {
  try {

    const { page, author } = req.query
    // pagination 

    const news = await db.query(`
         ${queryHits} WHERE hits.author = '${author}'
         ${paginate(page)} `
      , {
        type: QueryTypes.SELECT,
        plain: false,
      });
    res.json(news);
  } catch (error) {
    res.status(404).json({
      error: "Not found"

    });
  }
}

const getHitsByTag = async (req, res) => {

  try {

    const { page, tag } = req.query
    const news = await db.query(`
          ${queryHits}  WHERE  '${tag}' =any(hits._tags) 
          ${paginate(page)}`
      , {
        type: QueryTypes.SELECT,
        plain: false,

      });
    res.json(news);


  } catch (error) {
    res.status(404).json({
      error: "Not found"

    });
  }
}

const getHitsByTitle = async (req, res) => {
  try {

    const { page, title } = req.query

    const news = await db.query(`
              ${queryHits} WHERE story.story_title =' ${title}'
              ${paginate(page)}`
      , {
        type: QueryTypes.SELECT,
        plain: false,

      });
    res.json(news);


  } catch (error) {
    res.status(404).json({
      error: "Not found"

    });
  }


}

const deteteHits = async (req, res) => {
  // Get all hits from the database
  try {
    const  objectID  = req.params.objectID
    const news = await hits.destroy({
      where: {
        objectID: objectID
      }
    });
    res.json( objectID+' was deleted');
  } catch (error) {
    res.json({
      error: "Not was possible to delete"
    });
  }
}

module.exports = {
  getHits,
  getHitsByAuthor,
  getHitsByTag,
  getHitsByTitle,
  deteteHits
}