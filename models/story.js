
const {db  }= require("../db/connection");
const {  DataTypes } = require("sequelize");
const story = db.define("story", {
  story_title: { type: DataTypes.STRING },
  story_text: { type: DataTypes.TEXT },
  story_url: { type: DataTypes.STRING },
  },{freezeTableName: true,
    timestamps: false
  });
  
module.exports = story;





