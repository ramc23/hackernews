
const story = require("./story");
const {  DataTypes } = require("sequelize");
const { db } = require("../db/connection");

const hits = db.define("hits", {
    title: { type: DataTypes.STRING },
    objectID: { type: DataTypes.INTEGER, primaryKey: true },
    created_at: { type: DataTypes.DATE },
    points: { type: DataTypes.INTEGER },
    url: { type: DataTypes.STRING },
    parent_id: { type: DataTypes.INTEGER },
    num_comments: { type: DataTypes.INTEGER },
    created_at_i: { type: DataTypes.INTEGER },
    author: { type: DataTypes.STRING },
    comment_text: { type: DataTypes.TEXT },
    _tags: { type: DataTypes.ARRAY(DataTypes.STRING) },
    _highlightResult: { type: DataTypes.JSON },
}, {
    freezeTableName: true,
    timestamps: false
});


 story.hasMany(hits, {foreignKey: 'story_id', sourceKey: 'id'});
hits.belongsTo(story, {foreignKey: 'story_id', targetKey: 'id'});



 

/* 
author.hasOne(hits, {
    foreignKey: 'authorid'
});
hits.belongsTo(author);

story_title.hasOne(hits, {
    foreignKey: 'story_id'
});
hits.belongsTo(story_title);

comment_text.hasOne(hits, {
    foreignKey: 'commentid'
});
hits.belongsTo(comment_text);
 */


module.exports = hits;







