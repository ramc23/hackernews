
const story = require("../models/story");
const hits = require("../models/hits");
const { stories, hitsData ,getdatHit} = require("./data");
const { db } = require("./connection");

const checkDb = async () => {


    try {

   await getdatHit()

        await db.sync({ force: true });

        stories.forEach(async s => {
            await story.create({
                id: s.story_id,
                story_title: s.story_title,
                story_url: s.story_url,
                story_text: s.story_text,

            })
        })
        await hitsData.forEach(async h => {

            await hits.create({
                created_at: h.created_at,
                title: h.title,
                url: h.url,
                points: h.points,
                num_comments: h.num_comments,
                story_id: h.story_id,
                parent_id: h.parent_id,
                created_at_i: h.created_at_i,
                objectID: h.objectID,
                author: h.author,
                comment_text: h.comment_text,
                _tags: h._tags,
                _highlightResult: h._highlightResult
            })

        })

           
    
    } catch (error) {
        console.log(error);
    }
};

module.exports = { checkDb };

