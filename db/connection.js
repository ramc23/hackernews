const { Sequelize } = require('sequelize');

const db = new Sequelize(`postgres://${ process.env.PGUSER}:${process.env.PGPASSWORD}@${process.env.PGHOST}:${process.env.PGPORT}/${process.env.PGDATABASE}`);
try {
  db.authenticate();
  console.log('Connection has been established successfully.');


} catch (error) {
  console.error('Unable to connect to the database:', error);
}

module.exports = { db };