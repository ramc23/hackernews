const axios = require('axios')


const stories = [];
const hitsData = [];

  const getdatHit=  async () => { 

    try {
        const { data } =await axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
        data.hits.map(({
            created_at,
            title,
            url,
            author,
            points,
            story_text,
            comment_text,
            num_comments,
            story_id,
            story_title,
            story_url,
            parent_id,
            created_at_i,
            _tags,
            objectID,
            _highlightResult
        }) => {
    
            if (stories.filter(s => s.story_id === story_id && s.story_id != null).length === 0)
                stories.push({
                    story_id,
                    story_title: story_title || '',
                    story_url: story_url || '',
                    story_text: story_text || '',
                })
    
            hitsData.push({
                title,
                objectID,
                created_at,
                points,
                url,
                parent_id,
                created_at_i,
                num_comments,
                story_id,
                author,
                comment_text,
                _tags:_tags || [],
                _highlightResult
    
            })
    
        })
    
       
    } catch (error) {
        console.log(error);
    }



 }



module.exports = { stories, hitsData, getdatHit };