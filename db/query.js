queryHits = ` SELECT 
hits.created_at,
hits.title,
hits.url,
hits.author,
hits.points,
story.story_text,
hits.comment_text,
hits.num_comments,
hits.story_id,
story.story_title,
story.story_url,
hits.parent_id,
created_at_i,
hits."objectID",
hits._tags,
hits."_highlightResult"
FROM hits
inner join story on hits.story_id = story.id`
module.exports = {
    queryHits
}