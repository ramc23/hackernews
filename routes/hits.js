 const {Router}= require('express');
  const {
    getHits,
    getHitsByAuthor,
    getHitsByTag,
    getHitsByTitle,
    deteteHits  }= require('../controllers/hitController');

    const router = Router();
    router.get('/',getHits);
    router.get('/author',getHitsByAuthor);
    router.get('/:tag',getHitsByTag);
    router.get('/:title',getHitsByTitle);
    router.delete('/:objectID',deteteHits);
    module.exports=router;