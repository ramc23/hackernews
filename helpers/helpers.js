const paginate = (page) => {
    let pagination = ''
    if (page == undefined || page == null || page == '' || page == 1|| page == 0) {
      pagination = `LIMIT 5 OFFSET 0 `
    } else {
      pagination = ` LIMIT 5 OFFSET ${page * 5} `
    }
    return pagination
  }
  
  module.exports = {
    paginate
    }